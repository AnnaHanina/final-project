package webTest;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MyPostPage extends AbstractPage {

    @FindBy (xpath = ".//*[@href='/about']")
    private WebElement about;

    @FindBy (xpath = ".//*[@href='/contact']")
    private WebElement contact;

    @FindBy (xpath = ".//*[@href='/']")
    private WebElement home;

    @FindBy (xpath = ".//*[@href='/?page=2']")
    private WebElement nextPage;

    @FindBy (xpath = ".//*[@href='/?page=1']")
    private WebElement previousPage;

    @FindBy (xpath = ".//*[@href='/posts/15931']")
    private WebElement openPost;

    @FindBy (xpath = ".//*[@id='app']/main/div/div[2]/div[2]/div[1]/button")
    private WebElement order;

    @FindBy (xpath = ".//*[@id='SMUI-form-field-1']")
    private WebElement notMyPosts;

    public MyPostPage(WebDriver driver) {
        super(driver);
    }
    @FindBy (xpath = ".//*[@id='create-btn']")
    private WebElement create;


    public MyPostPage ClickAbout () {

        about.click();
        return this;
    }
    public MyPostPage ClickContact () {

        contact.click();
        return this;
    }
    public MyPostPage ClickHome () {

        home.click();
        return this;
    }
    public MyPostPage ClickNextPage () {

        nextPage.click();
        return this;
    }
    public MyPostPage ClickPreviousPage () {

        previousPage.click();
        return this;
    }

    public MyPostPage ClickOpenPost () {

        openPost.click();
        return this;
    }

    public MyPostPage ClickOrder () {

        order.click();
        return this;
    }

    public MyPostPage ClickNotMyPosts () {

        notMyPosts.click();
        return this;
    }

    public MyPostPage ClickCreate () {

        create.click();
        return this;
    }
}
