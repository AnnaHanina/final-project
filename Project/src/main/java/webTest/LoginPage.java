package webTest;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends AbstractPage {


    @FindBy(xpath = ".//*[@id='login']/div[1]/label/input")
    private WebElement inputUsername;

    @FindBy(xpath = ".//*[@id='login']/div[2]/label/input")
    private WebElement inputPassword;

    @FindBy(name = ".//*[@id='login']/div[3]/button")
    private WebElement LoginButton;


    public LoginPage(WebDriver driver) {
        super(driver);
    }

    private String login;
    private String password;

    public void LoginIn (String login, String password) throws InterruptedException {

        Actions LoginIn = new Actions(getDriver());
        LoginIn

                .click(this.inputUsername)
                .sendKeys(login)
                .click(this.inputPassword)
                .sendKeys(password)
                //.click(this.LoginButton)
                .click(this.inputPassword)
                .sendKeys(Keys.ENTER)
                .build()
                .perform();

    }

}
