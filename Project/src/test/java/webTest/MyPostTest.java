package webTest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class MyPostTest extends AbstractTest {


    @Test
    @DisplayName("Клик о нас")
    void ClickAbout() throws InterruptedException {
        LoginPage loginPage = new LoginPage (getDriver());
        loginPage.LoginIn("Anna", "97a9d330e2");
        MyPostPage myPostPage = new MyPostPage(getDriver());
        Thread.sleep(3000);
        myPostPage.ClickAbout();
        Thread.sleep(3000);
        Assertions.assertEquals("https://test-stand.gb.ru/about", getDriver().getCurrentUrl());
    }
    @Test
    @DisplayName("Клик контакты")
    void ClickContact() throws InterruptedException {
        LoginPage loginPage = new LoginPage (getDriver());
        loginPage.LoginIn("Anna", "97a9d330e2");
        MyPostPage myPostPage = new MyPostPage(getDriver());
        Thread.sleep(3000);
        myPostPage.ClickContact();
        Thread.sleep(3000);
        Assertions.assertEquals("https://test-stand.gb.ru/contact", getDriver().getCurrentUrl());
    }
    @Test
    @DisplayName("Клик дом")
    void ClickHome() throws InterruptedException {
        LoginPage loginPage = new LoginPage (getDriver());
        loginPage.LoginIn("Anna", "97a9d330e2");
        MyPostPage myPostPage = new MyPostPage(getDriver());
        Thread.sleep(3000);
        myPostPage.ClickHome();
        Thread.sleep(3000);
        Assertions.assertEquals("https://test-stand.gb.ru/", getDriver().getCurrentUrl());
    }

    @Test
    @DisplayName("Переход к следующей странице")
    void ClickNextPage() throws InterruptedException {
        LoginPage loginPage = new LoginPage (getDriver());
                loginPage.LoginIn("Anna", "97a9d330e2");
        MyPostPage myPostPage = new MyPostPage(getDriver());
        Thread.sleep(3000);
        myPostPage.ClickNextPage();
        Thread.sleep(3000);
        Assertions.assertEquals("https://test-stand.gb.ru/?page=2", getDriver().getCurrentUrl());
    }

    @Test
    @DisplayName("Переход к предыдущей странице")
    void ClickPreviousPage() throws InterruptedException {
        LoginPage loginPage = new LoginPage (getDriver());
        loginPage.LoginIn("Anna", "97a9d330e2");
        MyPostPage myPostPage = new MyPostPage(getDriver());
        Thread.sleep(3000);
        myPostPage.ClickNextPage();
        Thread.sleep(3000);
        myPostPage.ClickPreviousPage();
        Thread.sleep(3000);
        Assertions.assertEquals("https://test-stand.gb.ru/?page=1", getDriver().getCurrentUrl());
    }

    @Test
    @DisplayName("Переход к посту")
    void ClickOpenPost() throws InterruptedException {
        LoginPage loginPage = new LoginPage (getDriver());
        loginPage.LoginIn("Anna", "97a9d330e2");
        MyPostPage myPostPage = new MyPostPage(getDriver());
        Thread.sleep(3000);
        myPostPage.ClickOpenPost();
        Thread.sleep(3000);
        Assertions.assertEquals("https://test-stand.gb.ru/posts/15931", getDriver().getCurrentUrl());
    }

    @Test
    @DisplayName("Переход от новым постам к старым и обратно")
    void ClickOrder() throws InterruptedException {
        LoginPage loginPage = new LoginPage (getDriver());
        loginPage.LoginIn("Anna", "97a9d330e2");
        MyPostPage myPostPage = new MyPostPage(getDriver());
        Thread.sleep(3000);
        myPostPage.ClickOrder();
        Thread.sleep(3000);
        Assertions.assertEquals("https://test-stand.gb.ru/?sort=createdAt&order=DESC", getDriver().getCurrentUrl());
        myPostPage.ClickOrder();
        Assertions.assertEquals("https://test-stand.gb.ru/?sort=createdAt&order=ASC", getDriver().getCurrentUrl());

    }

    @Test
    @DisplayName("Переход к постам других пользователей и обратно")
    void ClickNotMyPosts() throws InterruptedException {
        LoginPage loginPage = new LoginPage (getDriver());
        loginPage.LoginIn("Anna", "97a9d330e2");
        MyPostPage myPostPage = new MyPostPage(getDriver());
        Thread.sleep(3000);
        myPostPage.ClickNotMyPosts();
        Thread.sleep(3000);
        Assertions.assertEquals("https://test-stand.gb.ru/?owner=notMe&sort=createdAt&order=ASC", getDriver().getCurrentUrl());
        myPostPage.ClickNotMyPosts();
        Assertions.assertEquals("https://test-stand.gb.ru/?sort=createdAt&order=ASC", getDriver().getCurrentUrl());

    }

    @Test
    @DisplayName("Переход к постам других пользователей и обратно")
    void ClickCreate() throws InterruptedException {
        LoginPage loginPage = new LoginPage (getDriver());
        loginPage.LoginIn("Anna", "97a9d330e2");
        MyPostPage myPostPage = new MyPostPage(getDriver());
        Thread.sleep(3000);
        myPostPage.ClickCreate();
        Thread.sleep(3000);
        Assertions.assertEquals("https://test-stand.gb.ru/posts/create", getDriver().getCurrentUrl());

    }
}