package webTest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class LoginTest extends AbstractTest{

    @Test
    @DisplayName("Авторизация валидные значения")

void LoginInPositive() throws InterruptedException {

new LoginPage (getDriver())
        .LoginIn("Anna", "97a9d330e2");
        Thread.sleep(5000);
        Assertions.assertEquals("https://test-stand.gb.ru/", getDriver().getCurrentUrl());
    }
    @Test
    @DisplayName("Авторизация невалидные значения")

    void LoginInNegative() throws InterruptedException {

        new LoginPage (getDriver())
                .LoginIn("Анна$", "97a9d330e2");
        Thread.sleep(5000);
        Assertions.assertEquals("https://test-stand.gb.ru/login", getDriver().getCurrentUrl());
    }
    @Test
    @DisplayName("Авторизация граничные значения 2 символа")

    void LoginNegativeLimitValues2() throws InterruptedException {

        new LoginPage (getDriver())
                .LoginIn("ss", "97a9d330e2");
        Thread.sleep(5000);
        Assertions.assertEquals("https://test-stand.gb.ru/login", getDriver().getCurrentUrl());
    }
    @Test
    @DisplayName("Авторизация граничные значения 3 символа")

    void LoginPositiveLimitValues3() throws InterruptedException {

        new LoginPage (getDriver())
                .LoginIn("a12", "97a9d330e2");
        Thread.sleep(5000);
        Assertions.assertEquals("https://test-stand.gb.ru/login", getDriver().getCurrentUrl());
    }
    @Test
    @DisplayName("Авторизация граничные значения 20 символов")

    void LoginPositiveLimitValues20() throws InterruptedException {

        new LoginPage (getDriver())
                .LoginIn("12345678912345678912", "97a9d330e2");
        Thread.sleep(5000);
        Assertions.assertEquals("https://test-stand.gb.ru/login", getDriver().getCurrentUrl());
    }
    @Test
    @DisplayName("Авторизация граничные значения 21 символ")

    void LoginNegativeLimitValues21() throws InterruptedException {

        new LoginPage (getDriver())
                .LoginIn("123456789123456789123", "97a9d330e2");
        Thread.sleep(5000);
        Assertions.assertEquals("https://test-stand.gb.ru/login", getDriver().getCurrentUrl());
    }
}
