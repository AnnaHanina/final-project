package backTest;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeAll;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public abstract class AbstractTest {

    static Properties prop = new Properties();
    private static InputStream configFile;
    private static String XAuthToken;
    private static String baseUrl;
    private static String baseurl1;
    private static String username;
    private static String password;
    protected static RequestSpecification requestSpecification;
    protected static ResponseSpecification responseSpecification;


    @BeforeAll
    static void initTest() throws IOException {
        configFile = new FileInputStream("src/main/resources/my.properties");
        prop.load(configFile);

        XAuthToken = prop.getProperty("XAuthToken");
        baseUrl = prop.getProperty("base_url");
        baseurl1 = prop.getProperty("base_url1");
        username = prop.getProperty("username");
        password = prop.getProperty("password");


        requestSpecification = new RequestSpecBuilder()
                .setContentType("application/x-www-form-urlencoded")
                .addHeader("X-Auth-Token", getXAuthToken())
                .log(LogDetail.ALL)
                .build();

        responseSpecification = new ResponseSpecBuilder()
                .expectResponseTime(Matchers.lessThan(5000L))
                .build();

        RestAssured.requestSpecification = requestSpecification;
        RestAssured.responseSpecification = responseSpecification;
    }

    public static String getXAuthToken() {return XAuthToken;}
    public static String getBaseUrl() {
        return baseUrl;
    }
    public static String getBaseUrl1() {return baseurl1;}
    public static String getUsername() {return username;}
    public static String getPassword() {return password;}
}