package backTest;

import org.junit.jupiter.api.Test;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;


public class PostAuthTest extends AbstractTest {

@Test
    public void validAuthTest() {
        given()
                        .formParam("username", getUsername())
                        .formParam("password", getPassword())
                        .contentType("application/x-www-form-urlencoded")
                        .when()
                        .post(getBaseUrl1()+"gateway/login")
                        .then()
                        .statusCode(200)
                        .body(containsString("token"));
    }

    @Test
    public void invalidAuthTest() {
        given()
                .formParam("username", "Анна$")
                .formParam("password", getPassword())
                .contentType("application/x-www-form-urlencoded")
                .when()
                .post(getBaseUrl1()+"gateway/login")
                .then()
                .statusCode(401)
                .body(containsString("Invalid credentials."));
    }

    @Test
    public void emptyFieldAuthTest() {
        given()
                .formParam("username", "")
                .formParam("password", "")
                .contentType("application/x-www-form-urlencoded")
                .when()
                .post(getBaseUrl1()+"gateway/login")
                .then()
                .statusCode(401)
                .body(containsString("Invalid credentials."));
    }
}
