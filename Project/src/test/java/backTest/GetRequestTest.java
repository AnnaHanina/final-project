package backTest;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;
import static io.restassured.RestAssured.given;

public class GetRequestTest extends AbstractTest {

    @Test
    void getTestSortNotMe() {

        given()
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .when()
                .get(getBaseUrl())
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON);
    }
    @Test
    void getTestAscendingOrderNotMe() {

        given()
                .queryParam("owner", "notMe")
                .queryParam("order", "ASC")
                .when()
                .get(getBaseUrl())
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON);
    }

    @Test
    void getTestDescendingOrderNotMe() {

        given()
                .queryParam("owner", "notMe")
                .queryParam("order", "DESC")
                .when()
                .get(getBaseUrl())
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON);
    }

    @Test
    void getTestOrderAllNotMe() {

        given()
                .queryParam("owner", "notMe")
                .queryParam("order", "ALL")
                .when()
                .get(getBaseUrl())
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON);
    }

    @Test
    void getTestNegativePageNotMe() {

        given()
                .queryParam("owner", "notMe")
                .queryParam("page", "-2")
                .when()
                .get(getBaseUrl())
                .then()
                .statusCode(400)
                .contentType(ContentType.JSON);
    }

    @Test
    void getTestSort() {

        given()
                .queryParam("sort", "createdAt")
                .when()
                .get(getBaseUrl())
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON);
    }
    @Test
    void getTestAscendingOrder () {

        given()
                .queryParam("order", "ASC")
                .when()
                .get(getBaseUrl())
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON);
    }

    @Test
    void getTestDescendingOrder() {

        given()
                .queryParam("order", "DESC")
                .when()
                .get(getBaseUrl())
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON);
    }

    @Test
    void getTestPage() {

        given()
                .queryParam("page", "2")
                .when()
                .get(getBaseUrl())
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON);
    }

    @Test
    void getTestPageAndOrder() {

        given()
                .queryParam("page", "2")
                .queryParam("order", "ASC")
                .when()
                .get(getBaseUrl())
                .then()
                .statusCode(200)
                .contentType(ContentType.JSON);
    }
}



